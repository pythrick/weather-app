from datetime import datetime

from django.core.management.base import BaseCommand
from django.db.models import Count
from django.db.models.functions import TruncDay

from apps.weather.models import Weather


class Command(BaseCommand):

    def handle(self, *args, **options):
        self.generate_report()

    def generate_report(self):
        """
        Method for generating CSV files with reports about the service usage, grouping by date and ip_address
        :return:
        """
        values = Weather.objects.annotate(
            date=TruncDay('created_at')).values("date", "ip_address").annotate(usage=Count('ip_address')).order_by('date')
        report_name = datetime.strftime(datetime.now(), "%Y%m%d%H%M%S")
        with open(f"exports/usage-report-{report_name}.csv", "w") as file:
            file.write("ip_address;date;usage\n")
            for item in list(values):
                file.write(
                    f"{item['ip_address']};{str(item['date'].date())};{item['usage']}\n"
                )
