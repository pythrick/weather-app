from django import forms

from apps.weather.models import Weather
from django.utils.translation import ugettext_lazy as _


class WeatherForm(forms.ModelForm):
    error_css_class = "is-invalid"

    location = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        help_text=_('You can enter any type of content, such as name of the city or zipcode.')
    )
    ip_address = forms.CharField(widget=forms.HiddenInput())

    class Meta:
        model = Weather
        fields = [
            'location',
            'ip_address',
        ]
