from datetime import timedelta
from django.utils.translation import ugettext_lazy as _

import environ  # Added to handle better with the environment variables applying 12-factor

root = environ.Path(__file__) - 2  # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False), )  # set default values and casting
environ.Env.read_env('.env')  # reading .env file
SITE_ROOT = root()

DEBUG = env('DEBUG', cast=bool)  # False if not in os.environ

DATABASES = {
    'default': env.db(),  # Raises ImproperlyConfigured exception if DATABASE_URL not in os.environ
}

public_root = root.path('public/')

ALLOWED_HOSTS = env('ALLOWED_HOSTS').split(',')

AUTH_USER_MODEL = 'user.User'

SECRET_KEY = env('SECRET_KEY')
SECURITY_PASSWORD_SALT = env('SECURITY_PASSWORD_SALT')

# Application definition

DJANGO_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles'
]

THIRD_PARTY = [
    'widget_tweaks',  # Added for styling form dynamically with bs4
    'raven.contrib.django.raven_compat',  # raven for monitoring the deployed app

]

PROJECT_APPS = [
    'apps.user',
    'apps.weather',
    'apps.main'
]

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY + PROJECT_APPS

MIDDLEWARE = [
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]


ROOT_URLCONF = 'config.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            'templates',
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'config.wsgi.application'

# Password validation
# https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGE_CODE = 'en-us'

LOCALE_PATHS = (
    root.path('locale').__str__(),
)

LANGUAGES = [
    ('en', _('English')),
]

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = False

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/
STATICFILES_DIRS = [
]

STATIC_URL = '/static/'

STATIC_ROOT = root.path('staticfiles').__str__()

GOOGLE_API_KEY = env('GOOGLE_API_KEY')
DARK_SKY_API_KEY = env('DARK_SKY_API_KEY')


GEOGRAPHIC_POSITION_URL = 'https://maps.googleapis.com/maps/api/geocode/json'
WEATHER_URL = 'https://api.darksky.net/forecast/'

SKIP_REAL = env.bool('SKIP_REAL', True)

if not DEBUG:
    RAVEN_CONFIG = {
       'dsn': env('SENTRY_DSN'),
    }