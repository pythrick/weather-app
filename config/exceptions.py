from django.core.exceptions import ValidationError


class APIProviderError(ValidationError):
    def __init__(self, msg, original_exception=Exception, status_code=None):
        super(APIProviderError, self).__init__({'message': msg}, status_code)
        self.original_exception = original_exception
